#!/usr/bin/env php
<?php

// no command line no party
if( !$argv ) {
	echo "This is a command line script.";
	exit(2);
}

// Read script arguments.
$in_bugzilla = $argv[1] ?? null;
$in_phab     = $argv[2] ?? null;
$batch_size  = $argv[3] ?? null;

// no first arguments, no party
if( !$in_bugzilla || !$in_phab ) {
	printf( "Usage: \n" );
	printf( "  %s WEIRD_BUGZILLA_DUMP.csv WEIRD_PHAB_DATASET.csv     [OPTIONAL_BATCH]\n", $argv[0] );
	printf( "  %s data/bugzilla.csv       data/phabricator-tasks.csv 100\n", $argv[0] );
	exit(1);
}

// no valid batch, no party
if ( $batch_size && !is_numeric( $batch_size ) ) {
	printf( "The batch size, if specified, must be numeric. Epic early fail." );
	exit( 2 );
}

// Index Phabricator PHIDs by their numeric ID.
$phab = [];
$handle_in_phab = fopen_strict($in_phab, 'r');
foreach(parse_csv_skipping_first($handle_in_phab) as $phab_row) {
	list($phab_id, $phab_phid) = $phab_row;
	$phab[$phab_id] = $phab_phid;
}
fclose($handle_in_phab);

// Associative map of statuses and their count.
$status_count = [];

// Let's say this into the beginning, so the query is shorter.
printf( "-- This script is intended to be executed using this database: phabricator_maniphest\n" );

// Loop bugzilla issues.
$handle_in_bugzilla = fopen_strict($in_bugzilla, 'r');
foreach(parse_csv_skipping_first($handle_in_bugzilla) as $bugzilla_row) {
	list($bugzilla_id, $bugzilla_date_raw, $bugzilla_user, $bugzilla_status) = $bugzilla_row;
	$bugzilla_id = (int) $bugzilla_id;

	$bugzilla_date = DateTime::createFromFormat('Y-m-d H:i:s T', $bugzilla_date_raw);
	$bugzilla_timestamp = $bugzilla_date->format('U');

	$expected_phab_id = 2000 + $bugzilla_id;

	// Only process BugZilla IDs matching a Phabricator Task in the related CSV provided by aklapper.
	// https://phabricator.wikimedia.org/T107254#9032265
	$expected_phab_phid = $phab[$expected_phab_id] ?? null;
	if( $expected_phab_phid === null ) {
		printf(
			"-- skipping BugZilla ID %d\n",
			$bugzilla_id
		);
		continue;
	}

	// Avoid usernames like "\n DROP TABLE ASD" lol
	kittenify_sql_comment($bugzilla_user);
	kittenify_sql_comment($bugzilla_date_raw);
	kittenify_sql_comment($bugzilla_status);

	// Finally, propose the patch :)
	printf(
		"UPDATE maniphest_task USE INDEX (PRIMARY) SET closedEpoch = %d WHERE closedEpoch IS NULL AND status NOT IN( 'open', 'stalled', 'duplicate', 'progress' ) AND id = %d; -- in date %s BugZilla ID %d closed by user %s with status %s\n",
		$bugzilla_timestamp,
		$expected_phab_id,
		$bugzilla_date_raw,
		$bugzilla_id,
		$bugzilla_user,
		$bugzilla_status
	);

	$status_count[ $bugzilla_status ] = $status_count[ $bugzilla_status ] ?? 0;
	$status_count[ $bugzilla_status ]++;
}

// Generate a nice recap for heroes that reaches the end of the file.
$RECAP_SPRINTF_FORMAT = "--   %15s ; %10s\n";
printf( "\n" );
printf( "-- A WINNER IS YOU! You reached the end of this file! [epic-winner-jingle.ogg]\n" );
printf( "-- YOU UNLOCKED +10 RELEASE ENG. EXPERIENCE POINTS!\n" );
printf( "-- YOU UNLOCKED THE FOLLOWING RECAP TABLE:\n" );
printf( $RECAP_SPRINTF_FORMAT, "Bugzilla Status", "Count" );
printf( $RECAP_SPRINTF_FORMAT, "---------------", "-----" );
arsort( $status_count, SORT_NUMERIC );
foreach( $status_count as $bugzilla_status => $count ) {
	printf( $RECAP_SPRINTF_FORMAT, $bugzilla_status, $count );
}

/**
 * Open a file or crash violently.
 * @param string $file File path
 * @param string $mode File modality
 * @return resource File handler from fopen()
 */
function fopen_strict($file, $mode) {
	$handle = fopen($file, $mode);
	if ($handle === false) {
		throw new Exception( sprintf(
			"Cannot open file %s",
			$file
		) );
	}
	return $handle;
}

function parse_csv_skipping_first($handle) {
	$i = 0;
	while (($data = fgetcsv($handle, 1000, ',')) !== false) {
		if ($i !== 0) {
			if( $data ) {
				yield $data;
			}
		}
		$i++;
	}
}

function kittenify_sql_comment(& $s) {
	$s = str_replace("\n", "", $s);
	$s = str_replace("--", "", $s);
}
